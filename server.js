const express = require('express');
const port = 3333;

const app = express();
app.set('views', './views');
app.set('view engine', 'pug');

app.get('/', (req, res) => {
  res.render('dashboard');
});

app.listen(port, () => {
  console.log(`server is running on port ${port}`);
});
